# GifMaker

Clone the repo:
```
git clone https://gitlab.com/Barvuz/gifmaker.git
```

Lastly - run the code
```
prog.c
```

## Name
Lior Lavie!

## Description
Code that takes from specific directory photos and run them as a gif with few options,
The code use opencv and run the gif automaticlly.

## Introduction
This is a gif maker, you might need to prepare some photos before you run the code.
This code use opencv so you might need to download it.

## Contact
Lior Lavie - liorlavie1@gmail.com