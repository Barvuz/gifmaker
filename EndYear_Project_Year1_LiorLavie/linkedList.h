#ifndef LINKEDLISTH
#define LINKEDLISTH

#define FALSE 0
#define TRUE !FALSE
#include <stdbool.h>
// Frame struct
typedef struct Frame
{
	char* name;
	unsigned int duration;
	char* path;
} Frame;


// Link (node) struct
typedef struct FrameNode
{
	Frame* frame;
	struct FrameNode* next;
} FrameNode;

FrameNode* create_frame(FrameNode* frame_list, int time, char* name, char* path);
FrameNode* delete_and_free(FrameNode* frame_list_head, char* name);
FrameNode* time_changer(FrameNode* frame_list_head, char* name);
FrameNode* location_changer(FrameNode* frame_list_head, char* name, int location);
FrameNode* load_project(char* path);
void save_project(char* path, FrameNode* gif);
#endif
