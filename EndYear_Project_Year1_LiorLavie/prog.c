/*********************************
* Class: MAGSHIMIM C2			 *
* openCV template      			 *
**********************************/

#define _CRT_SECURE_NO_WARNINGS

/*
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
*/

#include "LinkedList.h"
#include "view.h"

#define CV_IGNORE_DEBUG_BUILD_GUARD


#define SIZE 50
#define ERROR_CHOICE "Wrong type"

void print_menu();
void add_frame(FrameNode** gif); //function one
void delete_frame(FrameNode** list); //function two
void change_frame_location(FrameNode** list); //function three
void change_frame_time(FrameNode** list); //function four
void change_list_time(FrameNode** list); //function five
void print_frames(FrameNode* gif); //function six
void free_list(FrameNode** list);
void my_fgets(char* str);
bool check_exist(char* name);
bool check_location_exist(FrameNode* list, int location);
bool path_good(char* path); //no use to be easier to explain
FrameNode* reverse_gif(FrameNode* head);

int main(void)
{
	int choice = 0; //the choice
	char path[SIZE]; //for option 8
	FrameNode* gif = 0; //the gif
	FrameNode* temp_gif = 0; // for option 9
	do
	{
		printf("Please enter number\n[0]-new project\n[1]-load project\n");
		scanf("%d", &choice);
		getchar();
	} while (choice < 0 || choice > 1);

	if (choice == 1)
	{
		printf("Please enter path to load:\n");
		my_fgets(path);
		gif = load_project(path);
	}

	do
	{
		print_menu();
		printf("\nPlease enter number from the menu\n");
		scanf("%d", &choice);
		getchar();

		switch (choice)
		{
		case 0:
			printf("Bye bye :(");
			break;
		case 1:
			add_frame(&gif);
			break;

		case 2:
			delete_frame(&gif);
			break;

		case 3:
			change_frame_location(&gif);
			break;
		case 4:
			change_frame_time(&gif);
			break;

		case 5:
			change_list_time(&gif);
			break;

		case 6:
			print_frames(gif);
			break;

		case 7:
			play(gif);
			break;
		case 8:
			printf("Please enter path to save in:\n"); //scan path
			my_fgets(path);
			save_project(path, gif);
			break;

		case 9:
			temp_gif = reverse_gif(gif);
			play(temp_gif);
			break;

		default:
			printf(ERROR_CHOICE);
			break;

		}
	} 
	while (choice != 0);

	free_list(&gif);
	//printf("Leaks: %d", _CrtDumpMemoryLeaks());
	return 0;
}


/*
function: free the list (all the frame in the gif)
input: the location of the gif
output: none
*/
void free_list(FrameNode** list)
{
	FrameNode* curr = *list;
	FrameNode* temp = NULL;
	if (*list)
	{
		while (curr)  //for every frame
		{
			temp = curr->frame; //set frame
			free(temp); //clear frame
			temp = curr->next; //set as next
			free(curr); //clear node
			curr = temp; //set as next
		}
		list = NULL;
	}
}

/*
function: fgets and clean the \n
input: the str
output: none
*/
void my_fgets(char* str)
{
	fgets(str, SIZE, stdin);
	str[strcspn(str, "\n")] = 0;

	return str;
}

/*
function: check if the frame location exist
input: the gif and the frame name
output: true if exist, else false
*/
bool check_exist(FrameNode* list,char* name)
{
	FrameNode* temp = list;
	bool exist = false;
	while (temp)
	{
		if (strcmp(temp->frame->name, name) == 0) //if the name exist
		{
			exist = true;
		}
		temp = temp->next; //go to the next frame
	}

	return exist;
}

/*
function: check if the frame location exist
input: the gif and the frame wanted location
output: true if exist, else false
*/
bool check_location_exist(FrameNode* list, int location)
{
	int max = 1; //start from one becuase location start from one
	FrameNode* temp = list;

	while (temp)
	{
		max ++; //calculate max location
		temp = temp->next;
	}

	return(location >= 1 && location <= max); //the locations index start from 1
}

/*
function: check if the path is good
input: the path
output: true if good, else false
*/
bool path_good(char* path) //not use yet to be easier to check
{
	bool is_ok = true;
	//copied from the opencv test as example
	IplImage* image = cvLoadImage(path, 1);
	if (!image)//The image is empty.
	{
		is_ok = false;
	}
	return is_ok;
}


/*
function: print the menu
input: none
output: none
*/
void print_menu()
{
	printf("\n[0] Exit\n[1] Add new frame\n[2] Remove a frame\n[3] Change frame index\n[4] Change frame duration\n[5] Change duration of all frames\n[6] List frames\n[7] Play Movie!\n[8] Save project\n[9] play Movie backwoards");
}


/*
function: reverse the given gif and send the reversed version
input: the gif (without the reverse)
output: the reversed gif
*/
FrameNode* reverse_gif(FrameNode* head)
{
	FrameNode* previous = NULL;
	FrameNode* curr = head;
	FrameNode* next = NULL;
	while (curr != NULL) {
		// Store next
		next = curr->next;

		// Reverse curr node's pointer
		curr->next = previous;

		// Move pointers one position ahead.
		previous = curr;
		curr = next;
	}
	head = previous;
	return head;
}

/*
Function: move frame to location and change the locations so they will be in the correct order
Input: the gif
Output: none
*/
void change_frame_location(FrameNode** list)
{
	int location = 0;
	char* name = (char*)malloc(sizeof(char) * SIZE);
	do
	{
		printf("Please enter name: "); //scan the name
		my_fgets(name);
	} while (!check_exist(*list, name));

	do
	{
		printf("Please enter location: (1 - max)"); //scan the location
		scanf("%d", &location);
		getchar();

	} while (!check_location_exist(*list, location));

	location--; //set the location -- (because start from 1 and 1 = index 0)

	*list = location_changer(*list, name, location); //change the location

	free(name);
}

/*
Function: will create new node of frame
Input: the gif
Output: the new node for the next frame of the gif
*/
void add_frame(FrameNode** list)
{
	FrameNode* temp = *list;
	char* name = (char*)malloc(sizeof(char) * SIZE);
	int time = 0;
	char* path = (char*)malloc(sizeof(char) * SIZE);

	printf("Please enter name: "); //scan the name
	my_fgets(name);

	printf("Please enter time (in ms): "); //scan the time
	scanf("%d", &time);
	getchar();

	do
	{
		printf("Please enter path"); //scan the path
		my_fgets(path);
	} while (!path_good(path));

	if(temp)
	{
		while (temp->next) //go to the end of the list
		{
			temp = temp->next;
		}
		temp->next = create_frame(list, time, name, path); //create another frame
	}
	else
	{
		*list = create_frame(list, time, name, path); //create frame if list exist
	}

	printf("\n");
}

/*
Function: change the time of specific frame
Input:  the gif
Output: none
*/
void change_frame_time(FrameNode** list)
{
	char name[SIZE];
	int time = 0;
	do
	{
		printf("Please enter frame name:"); //scan the name
		my_fgets(name);
	} while (!check_exist(*list, name));

	do
	{
		printf("Please enter new time for the frame  (in ms): "); //scan the time
		scanf("%d", &time);
	} while (time <= 0);

	*list = time_changer(*list, name, time); //change the time
}

/*
Function: change the time of *every* frame
Input: the gif
Output: none
*/
void change_list_time(FrameNode** list)
{
	FrameNode* temp = *list;
	unsigned int time = 0;

	do
	{
		printf("Please enter new time for the frame (in ms): "); //scan the time
		scanf("%d", &time);
	} while (time <= 0);

	while (temp)
	{
		*list = time_changer(*list, temp->frame->name, time); //work for every frame
		temp = temp->next;
	}
}

/*
Function: delete frame
Input: the gif
Output: none
*/
void delete_frame(FrameNode** list)
{
	char name[SIZE];

	do
	{
		printf("Please enter frame name:"); //scan the name
		my_fgets(name);
	} while (!check_exist(*list, name)); 

	*list = delete_and_free(*list, name); //delete the name
}

/*
Function: print the frames one after another
Input: the gif
Output: none
*/
void print_frames(FrameNode* list)
{
	FrameNode* temp = list;
	printf("name\tduration\tpath\n"); //print the titles
	while(temp) //work for every frame
	{
		printf("%s\t%d ms\t\t%s\n", temp->frame->name, temp->frame->duration, temp->frame->path); //print the frame param
		temp = temp->next;
	}
	printf("\n");
}
