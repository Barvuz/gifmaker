#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "linkedList.h"

#define SIZE 50

/*
Function: create new node of the new frame
Input: the gif and the wanted time,name and path
output: new_node location
*/
FrameNode* create_frame(FrameNode* frame_list, int time, char* name, char* path)
{
	FrameNode* new_node = (FrameNode*)malloc(sizeof(FrameNode)); //add node
	Frame* frame = (Frame*)malloc(sizeof(Frame)); //add frame

	new_node->frame = frame; //set the node frame to frame
	new_node->frame->name = name; //set the node name to name
	new_node->frame->duration = time; //set the node time to time
	new_node->frame->path = path; //set the node path to path

	new_node->next = NULL; //set next as NULL

	return new_node;
}


/*
function: delete given name and return the deleted frame
input: the gif head and the name of the deleted one
output: the deleted node
*/
FrameNode* deleter(FrameNode** head, char* name)
{
	FrameNode* curr = *head;
	FrameNode* temp = NULL;

	if (!strcmp(curr->frame->name, name)) //if the name is the first
	{
		*head = (*head)->next; //go one step forward
		temp = curr; //temp set as the first frame
	}
	else
	{
		while (curr)
		{
			if (!strcmp(curr->next->frame->name, name)) //if its the next one
			{
				temp = curr->next;  //set temp as saver
				curr->next = curr->next->next; //connect between the two spaces
				break;
			}
			curr = curr->next; //go next
		}
	}
	return temp;
}

/*
Function: delete the frame and change the locations so they will be in the correct order
Input: the frame name and the gif
Output: the gif
*/
FrameNode* delete_and_free(FrameNode* frame_list_head, char* name, bool delete_and_return)
{
	FrameNode* temp = 0;

	temp = frame_list_head;

	temp = deleter(&frame_list_head, name);
	free(temp->frame); //free the frame that deleted
	free(temp);

	return frame_list_head;
}

/*
Function: change the cur frame to specific time
Input: the frame name and the wanted new time and the gif
Output: the gif
*/
FrameNode* time_changer(FrameNode* frame_list_head, char* name, unsigned int time)
{
	FrameNode* temp = 0;
	temp = frame_list_head;

	while (temp)
	{
		if (strcmp(temp->frame->name, name) == 0) //if its the one
		{
			temp->frame->duration = time; //set time
		}
		temp = temp->next;
	}

	return frame_list_head;
}

/*
Function: move frame to location and change the locations so they will be in the correct order
Input: the frame name, the wanted location and the gif
Output: the gif
*/
FrameNode* location_changer(FrameNode* frame_list_head, char* name, int location)
{
	FrameNode* temp = 0;
	FrameNode* temp_saver = 0;
	int i = 0;

	temp_saver = deleter(&frame_list_head, name); //give the deleted from function and delete the name from the gif
	temp_saver->next = NULL; //set next as NULL (for now to default)

	temp = frame_list_head;

	if (location > 0)
	{
		for (i = 0; i < location - 1; i++) //go to one before
		{
			temp = temp->next;
		}

		temp_saver->next = temp->next; //connect the temp_saver
		temp->next = temp_saver; //connect the regular
		temp = frame_list_head;
	}
	else
	{
		temp_saver->next = frame_list_head; //set as start
		temp = temp_saver;
	}
	

	return temp;
}


/*
function: save the project in the given path
input: the path and the gif
output: none (save in the path)
*/
void save_project(char* path, FrameNode* gif)
{
	FILE* gif_dir = 0;
	FrameNode* temp = gif;
	strcat(path, ".txt"); //add txt at the end
	gif_dir = fopen(path, "w"); //to create or delete everything that in the file (if exist just delete if no so open file)
	fclose(gif_dir);

	printf("\nPath is: %s\n", path);

	gif_dir = fopen(path, "a"); //open the dir
	while (temp)
	{
		fprintf(gif_dir, "%s\n%d\n%s", temp->frame->name, temp->frame->duration, temp->frame->path); //add line
		if (temp->next)
		{
			fprintf(gif_dir, "\n");
		}
		temp = temp->next;
	}
	fclose(gif_dir);
}


/*
function: load project from given path to file
input: the path of the file
output: the loaded gif
*/
FrameNode* load_project(char* path)
{
	int i = 0; //line counter
	FILE* gif_dir = 0;
	FrameNode* gif = 0, *temp = 0;
	char temp_str[SIZE] = { 0 };
	gif_dir = fopen(path, "r");

	if (gif_dir == NULL)
	{
		printf("Error: could not open file %s", gif_dir);
		return 0;
	}

	gif = create_frame(temp, 0, 0, 0); //sett at null cause change in the progress

	temp = gif;

	while (fgets(temp_str,SIZE,gif_dir)) //for every line
	{
		if (i % 3 == 0 && i != 0)
		{
			temp->next = create_frame(temp, 0, 0, 0); //sett at null cause change in the progress
			temp = temp->next;
		}

		printf("%s", temp_str);
		temp_str[strcspn(temp_str, "\n")] = 0;
		if (i % 3 == 0)
		{
			temp->frame->name = (char*)malloc(sizeof(char) * SIZE);
			strcpy(temp->frame->name, temp_str); //change the name
		}
		else if (i % 3 == 1)
		{
			temp->frame->duration = atoi(temp_str); //change the duration
		}
		else
		{
			temp->frame->path = (char*)malloc(sizeof(char) * SIZE);
			strcpy(temp->frame->path, temp_str); //change the path
		}
		
		i++; //go to next
	}

	return gif;
}